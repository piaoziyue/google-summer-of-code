---
title: Proposals forms 
slug: proposal-forms
author: Christian Frisson
date: 2022-04-05 09:24:11 UTC-04:00
tags: proposals, easy, 175 hours
type: text
---

<!-- Please follow and keep (do not delete) all of these comments! -->

We have simulated the workflow of registering as a GSoC contributor and submitting a proposal, so that:

* we better understand the requirements of this process as a GSoC organization,
* we are better equipped to onboard you potential GSoC contributors throughout the application process.

This process requires two steps to be completed, each with a form to fill:

1. Contributor Registration
2. Proposal Submission

Find in this page:

* fields that require input, containing wildcards `*` or ellipses `...`
* notes, starting with `Note:`

## Contributor Registration

This form starts here: [https://summerofcode.withgoogle.com/register/contributor](https://summerofcode.withgoogle.com/register/contributor)

### Page 1: GSoC Contributor Registration

Thank you for choosing to apply for Google Summer of Code! Please provide some information about yourself to get started. All fields are required unless stated otherwise.

#### Display Name *

> ...

This is your personal identifier that will be displayed on the site and in emails. It can be changed at any time in your profile.

#### Agreements and Rules

Click the links below and review the agreements to continue:

* [ ] [2022 Program Rules](https://summerofcode.withgoogle.com/rules)
* [ ] [2022 Contributor Agreement](https://summerofcode.withgoogle.com/terms/contributor)

#### Your Details

Information in this section will not be publicly visible. Google will be able to view this information to run the program. If you are accepted into the program, our payment processing vendor, Payoneer, will receive your name and country.

First Name *

> ...

Last Name *

> ...

First Name and Last Name must match the name you will use to receive payments.

Country *

> ...

The country you will reside in during the coding period.

* [ ] Opt in to join the GSoC Contributors/Students only email list
* [ ] Opt in to receive GSoC Alumni email updates

#### For Statistical Purposes

The following information is not publicly visible. Only Google can view this information to use for aggregate statistical purposes.

Gender *

* [ ] Female
* [ ] Male
* [ ] Nonbinary
* [ ] Prefer not to state

How did you first find out about Google Summer of Code? *

> ...

Have you applied to Google Summer of Code before?

* [ ] Yes
* [ ] No


Did you participate in [Google Code-in](http://g.co/gci) as a student?

* [ ] Yes
* [ ] No

Are you currently in an academic program (university, coding camp, online courses, etc.?)

* [ ] Yes
* [ ] No

Have you ever contributed to open source before?

* [ ] Yes, I have written code for an open source project. Note: If selected, a new input field appears and invites to "Please explain".
* [ ] Yes, I have participated in open source in other ways than coding. Note: If selected, a new input field appears and invites to "Please explain".
* [ ] No, this is my first time contributing to open source.

#### Settings

The following information will not be publicly visible and is only gathered to enhance your experience.


Timezone:
> ...

Program times will be converted to your timezone. [IANA Timezones](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones)

#### Register

* [ ] Register

You must accept the GSoC agreements before proceeding.

### Page 2: Your Google Summer of Code registration was completed successfully.

You can now submit up to three proposals to mentoring organizations.

[My dashboard](https://summerofcode.withgoogle.com/proposals)

## Submit Proposal

This form starts here: [https://summerofcode.withgoogle.com/programs/2022/organizations/society-for-arts-and-technology-sat/create-proposal](https://summerofcode.withgoogle.com/programs/2022/organizations/society-for-arts-and-technology-sat/create-proposal)

### Page 1: Submit Proposal

Please provide all required information and upload a PDF of your proposal.

Potential contributors may submit up to 3 proposals per program. Proposals may be changed or deleted until the submission deadline.

👉 The most successful proposals have been developed in collaboration with the target organization. Reach out to the organization, discuss, and get feedback, before and while writing your proposal to significantly increase your chances of being selected into the program.

#### Proposal Title *

> ...

#### Proposal Summary

Please provide a brief summary of your proposal. It should encompass the problem you are solving, how you plan to solve it, and a clear set of deliverables. If your proposal is accepted, this text will become your public project description on the program site.

> ...

#### Project size *

* [ ] Medium
* [ ] Large

Project size must match the size that the target organization expects. If you are proposing an idea from an organization list, it should match what is listed there.

Do not change the size (larger or smaller) without getting approval from the target organization first. It may result in your proposal being rejected.

#### Project Technologies *

Enter keywords for the primary technologies recommended for this idea. Examples: Python, Javascript, MySQL, Hadoop, OpenGL, Arduino
Project Technologies

> ...

#### Project Topics *

Enter keywords for general topics that describe this idea. Examples: Vision, Robotics, Cloud, Graphics, Web, Machine Learning
Project Topics

> ...

#### Proposal PDF *

Once a proposal has been uploaded, you may upload a new version of the proposal to replace the old version until April 19, 2022

* [ ] Choose file


#### Submit

* [ ] Submit proposal