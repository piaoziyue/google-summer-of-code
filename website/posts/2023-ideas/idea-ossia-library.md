<!--
.. title: Interfacing MediaCycle for media library exploration in ossia score
.. slug: idea-ossia-media-library
.. author: Jean-Michaël Celerier
.. date: 2023-03-15 08:20:00 UTC+01:00
.. tags: ideas, medium, 350 hours
.. type: text
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

Interfacing MediaCycle for media library exploration in ossia score

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

This project is about helping users of [ossia score](https://ossia.io/), a multimedia composition software to easily browse their media library, which may contain thousands of files, using [MediaCycle](https://github.com/MediaCycle/MediaCycle). 
Take for example sound collections: [MediaCycle](https://github.com/MediaCycle/MediaCycle) uses (currently unsupervised) machine learning techniques to organize sounds visually in a map according to their sonic similarity: the long, dark bass sounds would be close together, just like the short high-pitched trumpet sounds. 
While multiple environments of this sort already exist (like [AudioStellar](https://audiostellar.xyz/), [FluCoMa](https://github.com/flucoma), [MediaCycle](https://github.com/MediaCycle/MediaCycle)), only [MediaCycle](https://github.com/MediaCycle/MediaCycle) supports multimedia content (sounds, images, videos, 3D models, PDF documents). 
Both [MediaCycle](https://github.com/MediaCycle/MediaCycle) and [ossia score](https://ossia.io/) are cross-platform and implemented in C++ & Qt. [MediaCycle](https://github.com/MediaCycle/MediaCycle) needs a refresh of its code base. 

The goals of this project idea are to:

- review these media asset management solutions for inspiration, 
- implement communication mechanisms between [MediaCycle](https://github.com/MediaCycle/MediaCycle) and [ossia score](https://ossia.io/), 
- align the user experience of MediaCycle to be in par with [ossia score](https://ossia.io/).

## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

- Ensure that [MediaCycle](https://github.com/MediaCycle/MediaCycle) can be used by novice composers, by updating existing interactions such as hovering, drag'and'drop, etc.
- Create code that works across major operating systems (Linux Ubuntu 22.04 used at the SAT at Metalab, macOS, Windows). 
- Make multimedia artists happy :-)
- (Bonus) Create alternatives to the osg rendering components of [MediaCycle](https://github.com/MediaCycle/MediaCycle) to drop [OpenSceneGraph](https://github.com/MediaCycle/OpenSceneGraph) in favor of a newer open-source toolkit such as [VulkanSceneGraph](https://github.com/vsg-dev/VulkanSceneGraph), ideally compatible with AR/MR/VR/XR rendering and embeddable in Qt applications, among others to be reviewed during the contribution.

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 

- required: C++
- preferred: experience with Qt
- preferred: experience with AR/MR/VR/XR toolkits
- preferred: potentially AI and (unsupervised) machine learning skills

## Possible mentors

<!-- Please list each mentor hyperlinked to their gitlab profile. 2 possible mentors are more failsafe. -->

[Christian Frisson](https://gitlab.com/ChristianFrisson)
[Jean-Michaël Celerier](https://gitlab.com/jcelerier)

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

medium
