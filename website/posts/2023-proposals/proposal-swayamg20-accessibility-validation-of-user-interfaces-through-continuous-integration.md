---
title: Accessibility validation of user interfaces through continuous integration
slug: proposal-swayamg20-accessibility-validation-of-user-interfaces-through-continuous-integration
author: Swayam Gupta
date: 2023-02-18 11:24:11 UTC-05:00
tags: proposals, easy, 350 hours, Poire, Satellite, Scenic
type: text
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/writing-a-proposal
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with proposal-, then your your gitlab username and proposal title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: proposals, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

<!--
If you feel you must have graphical or interactive content associated with your application, please include this content in your merge request by prefixing the filenames with your page slug then followed by dash-separated words and embed this content with markdown syntax into your proposal. 
We prefer that you do not include of portrait picture of yourself yet, we want to avoid unintentional bias from reviewers. 
-->

<!--
We encourage participation from students from communities under-represented in the Science, Technology, Engineering, Arts, and Mathematics (STEAM) fields. We aim at increasing the participation of groups traditionally underrepresented in software development (including but not limited to: women, LGBTQ+, underrepresented minorities, and people with disabilities).
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Swayam Gupta
* website: ...
* gitlab username: @swayamg20
* timezone: GMT +5:30

<!-- Please do not share private contact information yet (email addresses, telephone number), we will ask you for this information later if your proposal is accepted. -->

## Title

<!-- 
Your title should be short, clear and interesting. 
The job of the title is to convince the reviewer to read your synopsis. 
Do not put your name in the title.
-->

Accessibility validation of user interfaces through continuous integration


## Synopsis

<!-- 
Start your proposal with a short summary, designed to convince the reviewer to read the rest of the proposal. 
Note that we expect the same content as for the Proposal Summary entry in your proposal submission form: website/posts/2022-proposals/proposal-forms.md
(150-200 words)
-->

Continuous Integration (CI) is a common software development methodology that entails integrating and testing code modifications on a frequent basis to ensure that they function correctly and meet the required quality standards. This procedure can be expanded to include accessibility testing of user interfaces, which can aid in identifying and resolving accessibility issues early in the development process.

The goal of this project is to develop an accessibility validation framework for user interfaces using SAT tools that can be integrated with existing CI pipelines. The SAT tools will identify accessibility issues and provide developers with feedback on how to remedy them by combining automated accessibility testing tools with manual testing.

The proposed project will comprise of the following tasks: (given that we already have SAT tools that can be integrated, we will only need to modify them to include accessibility testing in existing CI pipelines.

Development of guidelines and instruments for manual and automated testing of accessibility validation.
Integration of accessibility validation SAT tools into an influential open-source initiative.
Developer-friendly documentation of the framework and guidelines for accessibility validation.
This undertaking will result in a valuable contribution to the accessibility community and allow the student to gain experience in software development and accessibility testing.






## Benefits to Community

<!-- 
Make your case for a benefit to the organization, not just to yourself. 
Why would Google and SAT be proud to sponsor this work? 
How would open source or society as a whole benefit? 
What cool things would be demonstrated?
(150-200 words)
-->

This endeavour offers a variety of community benefits. The UI accessibility validation can improve the experience of rapid integration by automating tasks and incorporating multiple tests to improve the precision of results. There are numerous SAT tools that can be accessed through this initiative and will adhere to accessibility requirements. After perusing the SAT website, I discovered that there are numerous [SAT tools](https://sat-mtl.gitlab.io/en/)  such as [Poire](https://gitlab.com/sat-mtl/metalab/poire), [Scenic](https://gitlab.com/sat-mtl/tools/scenic/scenic), [Satellite](https://hub.satellite.sat.qc.ca/) that can be used in full stack development, but we must implement continuous integration to make them beneficial to other communities as well. 

This initiative will primarily enhance the accessibility of user interfaces, making them more usable for individuals with disabilities. By incorporating accessibility testing into the continuous integration process, developers can identify and resolve accessibility issues early in the development process, thereby reducing the likelihood of inaccessible user interfaces.
The accessibility validation framework SAT utility developed as part of this initiative will be an open-source contribution, accessible to all community members. This will serve as a valuable resource for organisations and developers seeking to enhance the accessibility of their user interfaces.
 The development of this initiative will entail collaboration between the student, the sat-mtl organization, and the larger community, providing an opportunity for the exchange of information and the formation of professional connections.
This GSoC project for the sat-mtl will have a positive impact on the community by increasing accessibility and highlighting the significance of designing and developing accessible user interfaces.

## Deliverables

<!-- 
Include a brief, clear work breakdown structure with milestones and deadlines. 
Make sure to label deliverables as optional or required. 
You may want your plan to start by producing some kind of technical paper, or planning the project in traditional Software Engineering style. 
It’s OK to include thinking time (“investigation”) in your work schedule. 
Deliverables should include investigation, coding and documentation.
(max 1000 words)
-->

1. Accessibility validation framework (Planning period)
2. Manual testing guidelines and tools
3. Integration with open-source project
4. Documentation
5. Code repository
6. Final report

#### Accessibility validation framework (week 1-3)
##### *Community bonding period (4th May, 2023 - 28th May, 2023)*
 A framework for accessibility testing of user interfaces that can be integrated into existing continuous integration pipelines. Since this framework is already build as SAT tools, I will be using few of the [SAT tools](https://sat-mtl.gitlab.io/en/ "SAT tools") for frontend like poire, satellite etc.
 The framework should utilize automated accessibility testing tools and manual testing to identify accessibility issues and provide feedback to developers on how to address them.

**Deliverables:**
- Project planning on how I will be starting.
- exploring the SAT tools in much deeper way. Research and investigating the frontend SAT tools.

#### Manual and automated testing guidelines and tools (week 4-5) May 29 - June 12
**Deliverables**:
- Set up guidelines and tools for manual and automated accessibility testing on user interface.

- Guidelines should cover a wide range of accessibility issues.
- Developing a proper manual for the guidelines.

#### Integration with SAT tools (week 6-9) 13 June - 11 July
breaking the assignment into various tasks.
**Deliverables**:
- Integration of the accessibility validation framework into an open-source project, which will serve as a demonstration of the framework's capabilities.
- Integrating Poire and different SAT Tools throught github CI, first target will be for React based tools like [Poire](https://gitlab.com/sat-mtl/metalab/poire "Poire"), [Satellite](https://hub.satellite.sat.qc.ca/ "Satellite"), [Scenic](https://gitlab.com/sat-mtl/tools/scenic/scenic "Scenic").
- Implement validation test caes following through testing guidelines and manual tests made.

#### Documentation (Week 10)
The documentation should be clear and easy to understand, even for developers who are not familiar with accessibility testing.
**Deliverables**:
- Comprehensive documentation for the accessibility validation framework, including installation instructions, usage guidelines, and troubleshooting tips.
- A proper documentation for all the completed tasks under the project.

#### Code repository (week 11)
**Deliverables**:
- Reviewing and pushing the build for the open source community.
- Including accessibility validation frameworks, manual testing guidelines, doccumentations.
-  A public code repository containing all the code developed as part of the project, including the accessibility validation framework, manual testing guidelines and tools, and documentation.

#### Final report (week 12)
-  A final report summarizing the project's goals, methodology, and outcomes, as well as recommendations for future work in the area of accessibility testing.- 



## Related Work

<!-- 
You should understand and communicate other people’s work that may be related to your own. 
Do your research, and make sure you understand how the project you are proposing fits into the target organization. 
Be sure to explain how the proposed work is different from similar related work.
(max 1000 words)
-->

**Comparing Web Accessibility Evaluation Tools:** This study examines web accessibility assessment tools by features, accuracy, and usability. The authors assess eight common accessibility evaluation tools and suggest the best tool for each use case. [link](https://www.researchgate.net/publication/338550460_Comparing_Web_Accessibility_Evaluation_Tools_and_Evaluating_the_Accessibility_of_Webpages_Proposed_Frameworks "link")

**Accessibility Checker:** A Tool to Support Web Accessibility Evaluation: Accessibility Checker is an open-source tool that helps developers and testers to evaluate the accessibility of web pages. The tool provides feedback on accessibility issues and suggests possible solutions. [link](https://www.w3.org/WAI/EO/Drafts/eval/tools2014/ "link")

**Accessibility Testing Automation Framework for Web Applications:** This paper presents an automation framework for accessibility testing of web applications. The framework integrates automated accessibility testing tools with continuous integration and delivery pipelines to ensure that accessibility issues are identified and resolved early in the development process.

- [NVDA (NonVisual Desktop Access)](https://www.nvaccess.org/ "NVDA (NonVisual Desktop Access)") : NonVisual Desktop Access (NVDA) is free, open-source Windows screen reader software that converts text into voice or Braille. NV Access created it and covers over 50 languages. NVDA is configurable and supports many apps and websites. It is used worldwide as an option to commercial screen readers.
- [WAVE](https://wave.webaim.org/ "WAVE") : WebAIM's free online WAVE tool evaluates web sites for accessibility issues. It checks web content for WCAG 2.1, Section 508, and ADA conformity. (ADA). WAVE flags possible accessibility issues, explains and suggests fixes, and produces clear reports. It helps web engineers, designers, and content makers make their webpages available to everyone, including people with disabilities.
- [Colour Contrast Analyser](https://www.tpgi.com/color-contrast-checker/ "Colour Contrast Analyser") : The Paciello Group's free Colour Contrast Analyser lets users assess web page and app foreground-background contrast. It helps web writers and artists meet WCAG 2.1 colour contrast criteria. The tool gives a pass/fail hue contrast rating, contrast ratio statistics, and accessibility suggestions. Windows, macOS, Chrome, and Firefox browser extensions offer the Colour Contrast Analyser.
- [Axe](https://www.deque.com/axe/ "Axe") : Deque Systems created open-source accessibility testing tool Axe. It helps writers and users find and fix web and app accessibility issues. Axe works as a browser plugin or with Selenium, Cypress, and TestCafe. It checks web content for compliance with accessibility standards like WCAG 2.1 and Section 508 and gives detailed reports on possible accessibility issues and solutions. Developers, designers, and usability workers worldwide use free Axe.
- [Pa11y](https://pa11y.org/ "Pa11y") : Pa11y is an open-source accessibility testing tool that helps developers and artists find and fix accessibility issues on websites and apps. It supports Selenium, Mocha, and Grunt and is easy to use. Pa11y analyses web content for conformity with accessibility standards like WCAG 2.1 and Section 508 and provides thorough reports on possible errors and proposed fixes. It's command-line or desktop-based. Developers, artists, and accessibility experts worldwide use Pa11y for free.

**Accessibility Evaluation of Web Applications Using Automated Tools**: This research article discusses the use of automated tools to evaluate the accessibility of web applications. The authors propose a framework that combines automated accessibility testing tools with manual testing to improve the accuracy of accessibility evaluation. [link](https://www.mdpi.com/2078-2489/11/1/40 "link")


## Biographical Information

<!-- 
Keep your personal info brief. 
Be sure to communicate personal experiences and skills that might be relevant to the project. 
Summarize your education, work, and open source experience. 
List your skills and give evidence of your qualifications. 
Convince your organization that you can do the work. 
Any published work, successful open source projects and the like should definitely be mentioned.
(max 200 words)
-->

I am a junior undergraduate from [IIT Kanpur](https://www.iitk.ac.in/ " IIT Kanpur"), India majoring in Material Science and Engineering.
I am a full stack developer and work mainly on frontend with ReactJs and including MERN stack. I have worked on various projects based on MERN Stack and nginx server. One of my projects got 300K views in over 2 months of time period. [link](https://techkriti.org/ "link")
Other then that I have also worked on python based web scrapping project, cybersecurity project etc. All of my projects can be found here on my [resume](https://drive.google.com/file/d/1PbmFX8sKlyqFxhdJPPbJm3Inl8_--e6n/view?usp=sharing "resume").

## Skills

<!-- 
Copy the list of required/preferred skills from the project idea and paste it below. 
For each skill, illustrate with your past experience how you are the best fit for your proposal.
The ideal proofs are factual: a short summary of selections from your past contributions, ideally with hyperlinks when applicable (including but not restricted to: source code repositories, pages from portfolio), with a clarification of your role and an explanation how these support the skills involved in your GSoC proposal.
-->

* required: Frontend stacks like ReactJS etc.
* preferred: experience with development languages and front-end stacks of the selected SAT tools UIs (for instance JavaScript and React for Poire and Scenic and Satellite)
* preferred: experience on Gitlab CI/CD


## Expected size of project 

<!--
Please write below this comment either: 175 hours or 350 hours
Please also adjust accordingly tags in post metadata.
-->


350 hours

## Rating of difficulty 

<!--
Please write below this comment either: easy, medium or hard
Please also adjust accordingly tags in post metadata.
 -->

easy
