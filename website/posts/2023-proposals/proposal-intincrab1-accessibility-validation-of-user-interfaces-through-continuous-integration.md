---
title: Accessibility validation of user interfaces of Poire, Satellite and Scenic through continuous integration
slug: proposal-intincrab1-accessibility-validation-of-user-interfaces-through-continuous-integration
author: Ajay Chauhan
date: 2023-02-18 11:24:11 UTC-05:00
tags: proposals, easy, 350 hours, Poire, Satellite, Scenic
type: text
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/writing-a-proposal
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with proposal-, then your your gitlab username and proposal title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: proposals, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

<!--
If you feel you must have graphical or interactive content associated with your application, please include this content in your merge request by prefixing the filenames with your page slug then followed by dash-separated words and embed this content with markdown syntax into your proposal. 
We prefer that you do not include of portrait picture of yourself yet, we want to avoid unintentional bias from reviewers. 
-->

<!--
We encourage participation from students from communities under-represented in the Science, Technology, Engineering, Arts, and Mathematics (STEAM) fields. We aim at increasing the participation of groups traditionally underrepresented in software development (including but not limited to: women, LGBTQ+, underrepresented minorities, and people with disabilities).
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Ajay Chauhan
* website: ...
* gitlab username: [@intincrab1](https://gitlab.com/intincrab1)
* timezone: GMT +5:30

<!-- Please do not share private contact information yet (email addresses, telephone number), we will ask you for this information later if your proposal is accepted. -->

## Title

<!-- 
Your title should be short, clear and interesting. 
The job of the title is to convince the reviewer to read your synopsis. 
Do not put your name in the title.
-->

Accessibility validation of user interfaces of Poire, Satellite and Scenic through continuous integration

## Synopsis

<!-- 
Start your proposal with a short summary, designed to convince the reviewer to read the rest of the proposal. 
Note that we expect the same content as for the Proposal Summary entry in your proposal submission form: website/posts/2022-proposals/proposal-forms.md
(150-200 words)
-->

Open Source tools, such as those developed by SAT, have been widely used for a variety of applications by academics and independent users,this project will help ensure that user interface developed at SAT are accessible to all users, including those with disabilities, and that accessibility issues are caught and addressed early in the development process and accessible interfaces ensure that people with disabilities are not excluded from accessing digital content, services, or products. This promotes inclusivity and helps to reduce barriers to participation.

This GSoC project attempts to implement validation tests during continuous integration with Gitlab CI/CD. The results of these tests will be reviewed and analyzed to identify accessibility issues, which will then be addressed through the implementation of appropriate fixes. These fixes will be based on best practices for accessibility as outlined in guidelines such as the [Web Content Accessibility Guidelines (WCAG)](https://www.w3.org/WAI/standards-guidelines/wcag/).

Accessibility checking using GitLab CI/CD involves automating the process of checking user interfaces for accessibility compliance. This can help ensure that web applications are accessible to all users, including those with disabilities, and that accessibility issues are caught and addressed early in the development process.

## Benefits to Community

<!-- 
Make your case for a benefit to the organization, not just to yourself. 
Why would Google and SAT be proud to sponsor this work? 
How would open source or society as a whole benefit? 
What cool things would be demonstrated?
(150-200 words)
-->

The project will benefit not only the organization but also society as a whole. By ensuring that the user interfaces of SAT tools meet accessibility standards, we will be able to provide equal opportunities for people with disabilities.

By sponsoring this work, Google and the SAT can demonstrate their commitment to accessibility and diversity. Open-sourcing the code for these validation tests can benefit the wider community, as it will allow other organizations and developers to implement similar tests and improve the accessibility of their own products.

The project will serve as an important step towards creating more inclusive technology and promoting greater access and engagement for all individuals, regardless of ability. The project will demonstrate the power of technology to create positive social impact and ensure that everyone has equal access to education and opportunities.

## Deliverables

<!-- 
Include a brief, clear work breakdown structure with milestones and deadlines. 
Make sure to label deliverables as optional or required. 
You may want your plan to start by producing some kind of technical paper, or planning the project in traditional Software Engineering style. 
It’s OK to include thinking time (“investigation”) in your work schedule. 
Deliverables should include investigation, coding and documentation.
(max 1000 words)
-->

### Milestones

* Define a set of validation tests for the SAT tools which includes Poire, Satellite and Scenic to ensure that they are working correctly and meeting the expected accessibility criteria.
* Identify any issues or failures that occur during the validation tests implement fixes to improve the accessibility of the SAT tools based on the validation test results and a review of accessibility guidelines, such as the Web Content Accessibility Guidelines (WCAG).
* Configure Gitlab CI/CD pipeline to automate the execution of the validation tests as part of the continuous integration process.

The order in which I plan to work on the project consists of three phases: Poire, Satellite, and Scenic. This order has been chosen based on various criteria, such as codebase size, community size and development history.

* The first phase of the project will involve working on Poire, which has the smallest codebase. This phase will be focused on familiarizing myself with the project structure and workflow, gaining experience working with the accessibility tools, and making initial contributions. By starting with Poire, I will be able to build momentum and confidence.
* The second phase will involve working on Satellite, which has a larger codebase and a larger community. I will build on my experience gained in the previous phase and tackle more complex issues. By working on Satellite, I will have the opportunity to make a more substantial impact on the project and contribute to its wider community.
* The final phase of the project will involve working on Scenic, which has the longest development history at the SAT, I will have gained significant experience and confidence   in contributing to the project by this time 

### Timeline

* **Pre GSOC Period**
    * Stay connected with the community
    * Learn more about accessibility and best practices
* **Community Bonding Period ( May 4, 2022 - May 28, 2022 )**
    * **May 4th, 2022 - May 11, 2022**
        * Get better acquainted with the mentors and other Contributors.
        * Discuss approaches to do the project 
        * Discuss and finalise the accessibility tools for the project
    * **May 11th, 2022 - May 28, 2022**
        * Explore the tools Poire, Satellite and Scenic in greater depth.
        * I would also start coding in this phase only, so that I get a head-start

* **May 29th - June 12th (Coding Period)**  
    * Write validation test scripts for Poire
    * Review accessibility guidelines and identify areas of improvement 

* **June 12th - June 26th 2022**  
    * Implement fixes to improve accessibility.
    * Implement continuous integration using Gitlab CI/CD for Poire

* **June 26th - July 10th 2022**   
    * Getting familiar with the codebase of Satellite and Mozilla Hubs
    * Create validation tests for Satellite
    * Review accessibility guidelines and identify areas of improvement 

* **July 10th  - July 24th 2022 (Phase 1 Evaluation)**  
    * Apply fixes to improve accessibility.
    * Implement continuous integration using Gitlab CI/CD for Satellite

* **July 24th - August 7th 2022**  
    * Getting familiar with the codebase of Scenic
    * Create validation tests for Scenic
    * Review accessibility guidelines and identify areas of improvement

* **August 7th - August 21st 2022**  
    * Implement fixes to improve accessibility.
    * Implement continuous integration using Gitlab CI/CD for Scenic

* **August 21st 2022 - August 28th 2022 (Final Week)**  
    * Finish any pending works in the project


## Related Work

<!-- 
You should understand and communicate other people’s work that may be related to your own. 
Do your research, and make sure you understand how the project you are proposing fits into the target organization. 
Be sure to explain how the proposed work is different from similar related work.
(max 1000 words)
-->

The goal of this project is to create validation tests during continuous integration with Gitlab CI/CD that will ensure that the user interfaces of Poire, Satellite and Scenic consistently meet accessibility standards. This is an important aspect of web development, as ensuring that web content is accessible to all users is critical for providing equal access to information and services for people with disabilities.

There are several related works and tools that focus on web accessibility testing and compliance. For example web accessibility testing tools like [axe](https://www.deque.com/axe/) by [Deque](https://www.deque.com/), [Accessibility Insights](https://accessibilityinsights.io/) by Microsoft, and [Tenon.io](https://tenon.io/).
In addition,there are several standards and guidelines for web accessibility, including the [Web Content Accessibility Guidelines (WCAG)](https://www.w3.org/WAI/standards-guidelines/wcag/) developed by the [World Wide Web Consortium (W3C)](https://www.w3.org). These guidelines provide a framework for making web content more accessible to people with disabilities. The guidelines cover a wide range of accessibility issues, such as text alternatives for non-text content, keyboard accessibility, color contrast, and more.

The proposed project is different from these related works in that it focuses specifically on implementing validation tests during continuous integration with Gitlab CI/CD. This approach enables automated testing of accessibility during the development process, which can save time and resources compared to manual testing. 
The project also include the process of applying fixes to improve accessibility based on validation tests, and a review of accessibility guidelines is an essential component of the project idea.

This process involves reviewing the accessibility guidelines and best practices, such as the Web Content Accessibility Guidelines (WCAG), and determining the most appropriate fixes for the identified issues. The fixes may involve modifying the user interface elements, such as adjusting color contrast, adding alt text to images, and ensuring keyboard navigation, among others.


## Biographical Information

<!-- 
Keep your personal info brief. 
Be sure to communicate personal experiences and skills that might be relevant to the project. 
Summarize your education, work, and open source experience. 
List your skills and give evidence of your qualifications. 
Convince your organization that you can do the work. 
Any published work, successful open source projects and the like should definitely be mentioned.
(max 200 words)
-->

My name is Ajay Chauhan, and I'm a second-year undergraduate student of Computer Science and Engineering. 
I'm a person of colour in STEM with a fascination towards arts and music. All of this made me gravitate toward SAT as my organisation of choice for GSoC, and I'm determined to use my experiences to make a positive impact.

I have always believed that "coders are the sorcerers of the 21st century". It conveys that they have the power to create tools for the upliftment of mankind, and what better way to do so than through open-source contributions. My experience with open source is relatively small, but it has been so exciting that I've been contributing to multiple projects once I got acquainted with the concept. Open-source contributions have made me realize the importance of well-documented and easy-to-understand code. I also have a little experience with competitive programming.

I am particularly interested in several areas of programming, including Algorithms, Operating Systems, and Web Development. Recently, I have also been exploring the fields of machine learning and Android development, broadening my knowledge and skills.
I started my programming journey with Web Development, which I studied in the [FullstackOpen](https://fullstackopen.com/en/) course offered by the University of Helsinki. Programming languages that I am well-versed in include C, C++, Javascript and Python, which are all part of my university curriculum. I have been actively exploring the Rust programming language for the past few months. ([source code](https://github.com/intincrab/rust-lang-book))

After learning about the project, I did further research on accessibility and read articles and talks related to it in order to get to know the best practices for creating an inclusive user interface.
Making music is one of my hobbies due to my fascination with art.

## Motivation

The proposed project aligns with my goals and presents an opportunity for me to develop my skills and gain experience.
I am excited about the opportunity to contribute to the development of more inclusive technology. As someone who values accessibility and believes in the importance of ensuring that all individuals have equal access to digital interfaces, this project aligns with my personal values and goals.
I have a keen interest in web development and user experience design, and I see this project as a chance to further develop my expertise in these areas. The project's focus on automation and continuous integration presents a valuable opportunity for me to enhance my technical skills and learn new tools and techniques.

Selecting SAT as my organisation of choice for the summer was driven by various factors. I became intrigued by the innovative ways in which art and technology are integrated to create beautiful works in SAT and how technology is applied in fields such as theatre and dance. This piqued my interest in the organization and I am excited to contribute to their efforts.

## Skills

<!-- 
Copy the list of required/preferred skills from the project idea and paste it below. 
For each skill, illustrate with your past experience how you are the best fit for your proposal.
The ideal proofs are factual: a short summary of selections from your past contributions, ideally with hyperlinks when applicable (including but not restricted to: source code repositories, pages from portfolio), with a clarification of your role and an explanation how these support the skills involved in your GSoC proposal.
-->

* **preferred:** experience with or willingness to learn development languages and front-end stacks of the selected SAT tools UIs (for instance JavaScript and React for Poire and Scenic and Satellite)
    * I have been using C/C++ and Python for various university projects for the past 1.5 year. ([source code](https://github.com/intincrab/banana-bot))
    * I have an adequate understanding of various development concepts including Git, MongoDB, React, NextJS, Express, Node and Javascript

* **preferred:** experience with or willingness to learn Gitlab CI/CD 
     * Once I stumbled upon the project, I started learning Gitlab CI/CD from Gitlab documentation and blogs.
     * I have completed a course on Git and Github ([credentials](https://www.udemy.com/certificate/UC-31e987ac-8064-4701-83d8-2c6423736da0/))

## Expected size of project 

<!--
Please write below this comment either: 175 hours or 350 hours
Please also adjust accordingly tags in post metadata.
-->


350 hours

## Rating of difficulty 

<!--
Please write below this comment either: easy, medium or hard
Please also adjust accordingly tags in post metadata.
 -->

easy
