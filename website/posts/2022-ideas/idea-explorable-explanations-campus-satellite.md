<!--
.. title: Explorable explanations for teaching digital arts concepts in hybrid telepresence campus
.. slug: idea-explorable-explanations-campus-satellite
.. author: Christian Frisson
.. date: 2022-02-19 17:01:11 UTC-05:00
.. tags: ideas, hard, 350 hours, Satellite
.. type: text
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

Explorable explanations for teaching digital arts concepts in hybrid telepresence campus settings

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

Explorable explanations add interativity to documents and figures, and associate illustrations and words to account for diverse abilities in learning complex concepts while playing with parameters. Examples are available here: [https://explorabl.es](https://explorabl.es)

Started in 2020, in response to the health crisis, the [Satellite hub](https://hub.satellite.sat.qc.ca/about/) building upon [Mozilla Hubs](https://github.com/mozilla/hubs/) is an immersive 3D social web environment developed at SAT to promote various cultural and artistic contents and to create synergy by interconnecting them, including for teaching in hybrid telepresence campus settings.

We want you to help us support teaching in hybrid telepresence settings by prototyping examplar explorable explanations and embedding these in Satellite / Mozilla Hubs including interactivity.

## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

* Prototype explorable explanations in JavaScript
* Integrate explorable explanations in [Mozilla Hubs](https://github.com/mozilla/hubs/) used for [Satellite](https://gitlab.com/sat-mtl/satellite/hubs-injection-server) by adapting [Spoke](https://github.com/mozilla/Spoke) to defining dynamic assets with interactivity.
* (Bonus) Present our joint contributions at an international conference.

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 

* required: experience with JavaScript
* preferred: understanding of the fundamentals of digital arts technologies, including but not limited to: computer graphics, computer vision, deep learning, digital audio signal processing, haptics

## Possible mentors

<!-- Please list yourself/yourselves. 2 possible mentors are more failsafe. -->

[Christian Frisson](https://gitlab.com/christianfrisson)

Mentors for this project idea can share their experience with potential GSoC contributors to this project idea in developing explorable explanations for various disciplines related to digital arts: 

* digital audio processing using processing.js: [The IDMIL Digital Audio Workbench](https://idmil.github.io/DAWb/)
<!-- Marcelo Wanderley, Travis West, Josh Rohs, Eduardo Meneses, and Christian Frisson. 2021. [The IDMIL Digital Audio Workbench](https://idmil.github.io/DAWb/): An interactive online application for teaching digital audio concepts. 16th AudioMostly Conference on Interaction with Sound. ACM. DOI: [10.1145/3478384.3478397](https://doi.org/10.1145/3478384.3478397) -->
* haptics using WebAudio in reveal.js slides: [WebAudioHaptics](https://WebAudioHaptics.github.io)
<!-- Christian Frisson, Thomas Pietrzak, Siyan Zhao, and Ali Israr. 2016. [WebAudioHaptics](https://WebAudioHaptics.github.io): Tutorial on Haptics with Web Audio. 2nd Web Audio Conference. WAC'16 -->
* information visualization using [Idyll](https://github.com/idyll-lang/)
<!-- Jagoda Walny, Christian Frisson, Mieka West, Doris Kosminsky, Søren Knudsen, Sheelagh Carpendale, Wesley Willett. 2020. Data Changes Everything: Challenges and Opportunities in Data Visualization Design Handoff. IEEE Transactions on Visualization and Computer Graphics 26, 1. TVCG. DOI: [10.1109/TVCG.2019.2934538](https://doi.org/10.1109/TVCG.2019.2934538) -->

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

hard
